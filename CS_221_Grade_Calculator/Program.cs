﻿namespace CS_221_Grade_Calculator
{
	#region Using Statements

	using System;
	using Gtk;

	#endregion Using Statements

	class MainClass
	{
		public static void Main (string[] args)
		{
			Application.Init ();
			MainWindow win = new MainWindow ();
			win.Show ();
			Application.Run ();
		}
	}
}
