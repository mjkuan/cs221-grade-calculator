﻿#region Using Statements

using System;
using Gtk;

#endregion Using Statements

public partial class MainWindow: Gtk.Window
{
	public MainWindow()
		: base(Gtk.WindowType.Toplevel)
	{
		Build();
	}

	protected void OnDeleteEvent(object sender, DeleteEventArgs a)
	{
		Application.Quit();
		a.RetVal = true;
	}

	protected void OnCalculateButtonReleased(object sender, EventArgs e)
	{
		double exam1Grade = 0;
		double exam2Grade = 0;
		double homeworkGrade = 0;

		double desiredGrade = 0;
		double finalExamGrade = 0;

		// Check if all the inputs are valid.
		if (!double.TryParse(Exam1Textbox.Text, out exam1Grade))
		{
			MessageDialog ms = new MessageDialog(null, 
				                   DialogFlags.Modal, 
				                   MessageType.Info, 
				                   ButtonsType.Close, 
				                   "The input is invalid for the Exam 1 grade.");
			ms.Run();
			ms.Destroy();
			return;
		}
		if (!double.TryParse(Exam2Textbox.Text, out exam2Grade))
		{
			MessageDialog ms = new MessageDialog(null, 
				                   DialogFlags.Modal, 
				                   MessageType.Info, 
				                   ButtonsType.Close, 
				                   "The input is invalid for the Exam 2 grade.");
			ms.Run();
			ms.Destroy();
			return;
		}
		if (!double.TryParse(HomeworkTextbox.Text, out homeworkGrade))
		{
			MessageDialog ms = new MessageDialog(null, 
				                   DialogFlags.Modal, 
				                   MessageType.Info, 
				                   ButtonsType.Close, 
				                   "The input is invalid for the Homework/Assignment grade.");
			ms.Run();
			ms.Destroy();
			return;
		}

		if (DesiredGradeRadioButton.Active)
		{
			if (!double.TryParse(FinalExamTextbox.Text, out finalExamGrade))
			{
				MessageDialog ms = new MessageDialog(null, 
					                   DialogFlags.Modal, 
					                   MessageType.Info, 
					                   ButtonsType.Close, 
					                   "The input is invalid for the Final Exam grade.");
				ms.Run();
				ms.Destroy();
				return;
			}
		}
		else if (FinalExamGradeRadioButton.Active)
		{
			if (!double.TryParse(DesiredGradeTextbox.Text, out desiredGrade))
			{
				MessageDialog ms = new MessageDialog(null, 
					                   DialogFlags.Modal, 
					                   MessageType.Info, 
					                   ButtonsType.Close, 
					                   "The input is invalid for your desired grade.");
				ms.Run();
				ms.Destroy();
				return;
			}
		}
			
		double weightedExam1Grade = exam1Grade * 0.15d;
		double weightedExam2Grade = exam2Grade * 0.15d;
		double weightedHomeworkGrade = homeworkGrade * 0.50d;

		double totalWeightedGrade = weightedExam1Grade + weightedExam2Grade +
		                            weightedHomeworkGrade;

		if (DesiredGradeRadioButton.Active)
		{
			double weightedFinalExamGrade = finalExamGrade * 0.20d;
			totalWeightedGrade += weightedFinalExamGrade;

			DesiredGradeTextbox.Text = (totalWeightedGrade).ToString();
		}
		else if (FinalExamGradeRadioButton.Active)
		{
			double weightedFinalExamGrade = desiredGrade - totalWeightedGrade;

			FinalExamTextbox.Text = (weightedFinalExamGrade / 0.20d).ToString();

		}
	}
}
